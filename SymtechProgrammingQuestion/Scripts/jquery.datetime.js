﻿(function ($) {
    $.fn.dateTime = function (options) {
        var settings = $.extend({}, $.fn.dateTime.defaults, options);
        return this.each(function () {
            $.dateTime(this, settings);
        });
    };

    $.dateTime = function (elem, settings) {
        return elem.dateTime || (elem.dateTime = new jQuery._dateTime(elem, settings));
    };

    $._dateTime = function (elem, settings) {
        setTimeVal(elem, $panel, settings);

        var $panel = $('<div class="datetime"></div>');
        var $panelDate = $('<div><label>Date</label><div><input type="date" value="' + elem.date +'"/></div></div>');
        var $panelTime = $('<div><label>Time</label><div><input type="time" value="' + elem.time +'"/></div></div>');
        var $panelButton = $('<div><input type="button" value="Confirm"/></div>');

        $panel.addClass("");

        $("input", $panelButton).click(function () {
            var newDate = $("input", $panelDate).val();
            var newTime = $("input", $panelTime).val();

            $(elem).val(newDate + " " + newTime);

            $panel.hide();
        });

        $panel.append($panelDate);
        $panel.append($panelTime);
        $panel.append($panelButton);
        $panel.appendTo('body').hide();
        
        showDateTime = function () {
            if ($panel.is(":visible")) {
                $panel.hide();
                return;
            }
            var elemOffset = $(elem).offset();
            $panel.css({ 'top': elemOffset.top + elem.offsetHeight, 'left': elemOffset.left });
            $panel.show();

            return;
        };
        $(elem).click(showDateTime);
        
    };

    $.fn.dateTime.defaults = {
        defaultDateTime: defaultDateString()
    };

    function defaultDateString() {
        var date = new Date();
        
        return date.getFullYear() + "-"
            + Appendzero(date.getMonth() + 1) + "-"
            + Appendzero(date.getDate()) + " "
            + Appendzero(date.getHours()) + ":"
            + Appendzero(date.getMinutes()) + ":"
            + Appendzero(date.getSeconds());
    }

    function setTimeVal(elem, $panel, settings) {
        elem.value = $(elem).val();

        if (!IsDateTime(elem.value)) {
            elem.value = settings.defaultDateTime;
        }

        var date = new Date(Date.parse(elem.value));
        elem.date = date.getFullYear() + "-"
            + Appendzero(date.getMonth() + 1) + "-"
            + Appendzero(date.getDate());
        elem.time = Appendzero(date.getHours()) + ":"
            + Appendzero(date.getMinutes()) + ":"
            + Appendzero(date.getSeconds());

        $(elem).change();
    }

    function Appendzero(obj) {
        if (obj < 10) return "0" + obj;
        else return obj;
    }

    function IsDateTime(dateString) {
        if (dateString.trim() === "") {
            return false;
        }

        var r = dateString.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/); 

        if (r === null) {
            return false;
        }

        var d = new Date(r[1], r[3] - 1, r[4], r[5], r[6], r[7]);
        return d.getFullYear() == r[1]
            && d.getMonth() == r[3] - 1
            && d.getDate() == r[4]
            && d.getHours() == r[5]
            && d.getMinutes() == r[6]
            && d.getSeconds() == r[7];
    }

}(jQuery));